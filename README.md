# Arduino Bottle Filler
This repository contains the source-code for an arduino that is used in a
bottle filling machine.

The code was derived from the tutorial here [here](https://www.instructables.com/Arduino-Bottlefiller/)

The code was restructured to fix indentation issues, rewritten to English, so
the code is more readable, and variables were renamed using the Google C++
style guide.