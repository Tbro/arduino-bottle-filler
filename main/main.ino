// Define pin numbers of components
const int kPinValve1 = 2;
const int kPinValve2 = 3;
const int kPinValve3 = 4;
const int kPinValve4 = 5;
const int kPinValve5 = 6;
const int kPinValve6 = 7;

const int kPinPump = 23;

const int kPinButton1 = 33;
const int kPinButton2 = 35;
const int kPinButton3 = 37;
const int kPinButton4 = 39;
const int kPinButton5 = 41;
const int kPinButton6 = 43;

const int kPinBottle1Full = 0;
const int kPinBottle2Full = 1;
const int kPinBottle3Full = 2;
const int kPinBottle4Full = 3;
const int kPinBottle5Full = 4;
const int kPinBottle6Full = 5;

const int bottle_full_limit = 500;

// Variables used in the program
int bottle_1_full_value = 0;
int bottle_2_full_value = 0;
int bottle_3_full_value = 0;
int bottle_4_full_value = 0;
int bottle_5_full_value = 0;
int bottle_6_full_value = 0;

boolean valve_1_enabled;
boolean valve_2_enabled;
boolean valve_3_enabled;
boolean valve_4_enabled;
boolean valve_5_enabled;
boolean valve_6_enabled;

boolean pump_enabled;

/*
Initialize arduino
*/
void setup() {
  // Initialize the Command line output
  Serial.begin(9600);
  Serial.println("Initializing Controller");
  
  // Configure each of the digital pins as ieter input or output
  pinMode(kPinValve1, OUTPUT);
  pinMode(kPinValve2, OUTPUT);
  pinMode(kPinValve3, OUTPUT);
  pinMode(kPinValve4, OUTPUT);
  pinMode(kPinValve5, OUTPUT);
  pinMode(kPinValve6, OUTPUT);

  pinMode(kPinButton1, INPUT_PULLUP);
  pinMode(kPinButton2, INPUT_PULLUP);
  pinMode(kPinButton3, INPUT_PULLUP);
  pinMode(kPinButton4, INPUT_PULLUP);
  pinMode(kPinButton5, INPUT_PULLUP);
  pinMode(kPinButton6, INPUT_PULLUP);

  pinMode(kPinPump, OUTPUT);

  // Initially, all valves and the pump are disabled
  valve_1_enabled = false;
  valve_2_enabled = false;
  valve_3_enabled = false;
  valve_4_enabled = false;
  valve_5_enabled = false;
  valve_6_enabled = false;

  pump_enabled = false;
}

/*
During operation, this code is run in an infinite loop
*/
void loop() {
  // Read the values of the bottle full pins (value between 0 and 1024, steps between 0 and 5v)
  bottle_1_full_value = analogRead(kPinBottle1Full);
  bottle_2_full_value = analogRead(kPinBottle2Full);
  bottle_3_full_value = analogRead(kPinBottle3Full);
  bottle_4_full_value = analogRead(kPinBottle4Full);
  bottle_5_full_value = analogRead(kPinBottle5Full);
  bottle_6_full_value = analogRead(kPinBottle6Full);

  // If 'Enable button' is pressed (== LOW) and there's no overfill, set the valve as enabled
  if ((digitalRead(kPinButton1)==LOW)&&((bottle_1_full_value)<bottle_full_limit)){
    valve_1_enabled = true;
    Serial.println("Bottle 1 Present, not full");
  } else {
    valve_1_enabled = false;
    Serial.println("Bottle 1 absent or full");
  }

  if ((digitalRead(kPinButton2)==LOW)&&((bottle_2_full_value)<bottle_full_limit)){
    valve_2_enabled = true;
    Serial.println("Bottle 2 Present, not full");
  } else {
    valve_2_enabled = false;
    Serial.println("Bottle 2 absent or full");
  }

  if ((digitalRead(kPinButton3)==LOW)&&((bottle_3_full_value)<bottle_full_limit)){
    valve_3_enabled = true;
    Serial.println("Bottle 3 Present, not full");
  } else {
    valve_3_enabled = false;
    Serial.println("Bottle 3 absent or full");
  }

  if ((digitalRead(kPinButton4)==LOW)&&((bottle_4_full_value)<bottle_full_limit)){
    valve_4_enabled = true;
    Serial.println("Bottle 4 Present, not full");
  } else {
    valve_4_enabled = false;
    Serial.println("Bottle 4 absent or full");
  }

  if ((digitalRead(kPinButton5)==LOW)&&((bottle_5_full_value)<bottle_full_limit)){
    valve_5_enabled = true;
    Serial.println("Bottle 5 Present, not full");
  } else {
    valve_5_enabled = false;
    Serial.println("Bottle 5 absent or full");
  }

  if ((digitalRead(kPinButton6)==LOW)&&((bottle_6_full_value)<bottle_full_limit)){
    valve_6_enabled = true;
    Serial.println("Bottle 6 Present, not full");
  }
  else {
    valve_6_enabled = false;
    Serial.println("Bottle 6 absent or full");
  }

  if ((valve_1_enabled == true)||(valve_2_enabled == true)||(valve_3_enabled == true)||(valve_4_enabled == true)||(valve_5_enabled == true)||(valve_6_enabled == true)){
    pump_enabled = true;
  } else {
    pump_enabled = false;
  }

  // If valve is set as enabled, put voltage on pin
  if (valve_1_enabled == true){
    digitalWrite(kPinValve1, HIGH);
    Serial.println("Valve 1 Open");
  } else {
    digitalWrite(kPinValve1, LOW);
    Serial.println("Valve 1 Closed");
  }

  if (valve_2_enabled == true){
    digitalWrite(kPinValve2, HIGH);
    Serial.println("Valve 2 Open");
  } else {
    digitalWrite(kPinValve2, LOW);
    Serial.println("Valve 2 Closed");
  }
  
  if (valve_3_enabled == true){
    digitalWrite(kPinValve3, HIGH);
    Serial.println("Valve 3 Open");
  } else {
    digitalWrite(kPinValve3, LOW);
    Serial.println("Valve 3 Closed");
  }
  
  if (valve_4_enabled == true){
    digitalWrite(kPinValve4, HIGH);
    Serial.println("Valve 4 Open");
  } else {
    digitalWrite(kPinValve4, LOW);
    Serial.println("Valve 4 Closed");
  }
  
  if (valve_5_enabled == true){
    digitalWrite(kPinValve5, HIGH);
    Serial.println("Valve 5 Open");
  } else {
    digitalWrite(kPinValve5, LOW);
    Serial.println("Valve 5 Closed");
  }
  
  if (valve_6_enabled == true){
    digitalWrite(kPinValve6, HIGH);
    Serial.println("Valve 6 Open");
  } else {
    digitalWrite(kPinValve6, LOW);
    Serial.println("Valve 6 Closed");
  }

  if (pump_enabled == true){
    Serial.println("Pump on");
    digitalWrite(kPinPump, HIGH);
  } else {
    digitalWrite(kPinPump, LOW);
    Serial.println("Pump off");
  }
    
  // Print all values of the overfills
  Serial.println("Valve overfill valve 1:");
  Serial.println(bottle_1_full_value);

  Serial.println("Valve overfill valve 2:");
  Serial.println(bottle_2_full_value);

  Serial.println("Valve overfill valve 3:");
  Serial.println(bottle_3_full_value);

  Serial.println("Valve overfill valve 4:");
  Serial.println(bottle_4_full_value);

  Serial.println("Valve overfill valve 5:");
  Serial.println(bottle_5_full_value);

  Serial.println("Valve overfill valve 6:");
  Serial.println(bottle_6_full_value);

  delay(500);

}
